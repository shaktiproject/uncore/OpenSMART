import Fifo::*;
import Types::*;
import VirtualChannelTypes::*;
import CreditTypes::*;
import SpecialFIFOs ::*;
import MessageTypes::*;
import Vector::*;
import FIFOF::*;

interface ReverseCreditUnit;
  /* Credit */
  method Action putCredit(CreditSignal credit);
  `ifdef SMART
  method Action putCreditSMART(CreditSignal credit);
  `endif
  method ActionValue#(CreditSignal) getCredit;
endinterface

// TODO
// 2 explore:
// mkPipelineFifo with depth 1
// normal FIFO with depth 2
(* synthesize *)
module mkReverseCreditUnit(ReverseCreditUnit);
//  Fifo#(NumVCs, CreditSignal)     creditQueue <- mkPipelineFifo;
//	FIFOF#(CreditSignal) creditQueue <- mkPipelineFIFOF;

/// NumVNETs is the total number of VNET's, like 3 VNETs
///

  Vector#(NumVNETs,FIFOF#(CreditSignal)) creditQueue <- replicateM(mkSizedFIFOF(2));
  Fifo#(NumVCs, CreditSignal)     smartCreditQueue <- mkPipelineFifo;
  Reg#(MsgType) msgType <- mkReg(0); // arbitrary initializing with Data message type

//for(Integer i = 0; i < 3; i = i+1) begin
  rule rl_display_cq;
    for(Integer i = 0; i < 3; i = i+1) begin
      if(creditQueue[i].notEmpty)begin
        let lv_credit = creditQueue[i].first();
        $display($time, "\t credit queue contetnt for VNET: %d is :", i, fshow(lv_credit));
      end
    end
  endrule
//end


  method Action putCredit(CreditSignal credit);
    if(isValid(credit)) begin
      /*
      let msgType = credit.msgType;

      ///enqueue into correct creditQueue based on message class
      if(msgType==Data) begin
        creditQueue[0].enq(credit);
      end

      else if (msgType==Control0)begin
        creditQueue[1].enq(credit);
      end

      else begin
        creditQueue[2].enq(credit);
      end
      */

      let lv_msgType = validValue(credit).msgType;
      //let lv_vnet = getVnetID(lv_msgType);
      let lv_vnet =(lv_msgType);
      creditQueue[lv_vnet].enq(credit);
      msgType <= lv_msgType;

      //$display($time, "\tputCredit happening!, for credit : ", fshow(credit));

    end
  endmethod




`ifdef SMART
  method Action putCreditSMART(CreditSignal credit);
    if(isValid(credit)) begin
      smartCreditQueue.enq(credit);
    end
  endmethod
`endif

// Cyele PATH breaks.. !
// creditQueue is a pipeline FIFO.
// putCredit and getCredit can't happen in the same cycle for a empty creditQueue.
// essentially, the number of entries used in creditQueue are either 0 or 1.
// later the enq and deq happens together but for different flits.
  method ActionValue#(CreditSignal) getCredit;
    CreditSignal credit = Invalid;

    let lv_msgType = msgType;
    //let lv_vnet = getVnetID(lv_msgType);
    let lv_vnet = (lv_msgType);
    //let lv_vnet = 2;
    $display($time,"\tinside getCredit!!!!");
    if(creditQueue[lv_vnet].notEmpty) begin
      creditQueue[lv_vnet].deq;
      credit = creditQueue[lv_vnet].first();
      $display($time, "\tgetCredit is happening!, for credit: ", fshow(credit));
    end
    //else if(smartCreditQueue.notEmpty) begin
    //  smartCreditQueue.deq;
    //  credit = smartCreditQueue.first();
    //end
    //else begin
    //  credit = Invalid;
    //end

    return credit;
  endmethod

endmodule
