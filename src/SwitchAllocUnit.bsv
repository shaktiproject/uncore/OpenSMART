import Vector::*;
import Fifo::*;

import Types::*;
import VirtualChannelTypes::*;
import MessageTypes::*;
import SwitchAllocTypes::*;
import NtkArbiter::*;

interface SwitchAllocUnit;
  method Bool isInited;
  method Action reqSA(SAReq req, FreeVCInfo freeVCInfo,VNETreq vnet);
  method ActionValue#(SARes) getGrantedInPorts;
`ifdef SMART
  method ActionValue#(SARes) getGrantedOutPorts;
`endif
endinterface


// Arbiter gives the result in type Direction (Bit#(NumPorts))
// It represents the winner index in input sides.
// Ex) If a flit from north input port won the output switch toward east
//                                                     LWSEN
//       => When (outPort = dIdxEast), partialRes = 5'b00001

// By OR-ing every partial result, we get the bit representation of winners.
// Ex) If flits from N, S, W port won the switch
//                  LWSEN
//    => saRes = 5'b01101



(* synthesize *)
module mkSwitchAllocUnit(SwitchAllocUnit);

  /********************************* States *************************************/

  Reg#(Bool)                            inited             <- mkReg(False);
  Fifo#(1, SAReq)                       saReqBuf           <- mkBypassFifo;
  Fifo#(1, VNETreq)                     vNETreqBuf         <- mkBypassFifo;
  Fifo#(1, FreeVCInfo)                  freeVCInfoBuf      <- mkBypassFifo;
  Fifo#(1, SARes)                       grantedInPortsBuf  <- mkBypassFifo;
`ifdef SMART
  Fifo#(1, SARes)                       grantedOutPortsBuf <- mkBypassFifo;
`endif

  /******************************* Submodules ***********************************/
  Vector#(NumPorts, NtkArbiter#(NumPorts)) outPortArbiter  <- replicateM(mkOutPortArbiter);

  /******************************* Functions ***********************************/
  function SAReqBits genArbitReqBits(SAReq saReq, FreeVCInfo freeVCInfo, VNETreq vnet);
    //actionvalue
  //Arbitration request bits are the transpose of SA request bits
  //If there is no avaialble VC, blocks the request
  //  => This increases the critical path, but it saves cycles
  //     as it prevents failures due to VC availability after SA

    SAReqBits saReqBits = newVector;

    for(Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort + 1) begin
      for(Integer inPort = 0; inPort < valueOf(NumPorts); inPort = inPort + 1) begin
        //saReqBits[outPort][inPort] = (freeVCInfo[outPort]==1)? saReq[inPort][outPort] : 0;

        /// vnet has information about the vnet's of winner input port arbitration flits
        /// freeVCInfo has information about available free VNET's at each output port
        /// need to match both
        let lv_inport_vnet = (vnet[inPort]);
        if(freeVCInfo[outPort][lv_inport_vnet]=='b1)begin
          //$display("You are inside SAReqBits, required VC is present at downstream router");
          saReqBits[outPort][inPort] = saReq[inPort][outPort];
        end
        else begin
          saReqBits[outPort][inPort] = 0;
        end

      end
    end

    return saReqBits;

    //endactionvalue
  endfunction

  /************************* Initialization Behavior ***************************/
  rule doInitialize(!inited);
    inited <= True;

    for(Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort+1) begin
      outPortArbiter[outPort].initialize;
    end
  endrule


  rule doSA(inited && saReqBuf.notEmpty);

    VNETreq vnet = vNETreqBuf.first;
    vNETreqBuf.deq;

    SAReq req = saReqBuf.first;
    saReqBuf.deq;

    FreeVCInfo freeVCInfo = freeVCInfoBuf.first;
    freeVCInfoBuf.deq;

    SARes grantedInPorts = 0;
    SARes grantedOutPorts = 0;

    let saReqBits = genArbitReqBits(req, freeVCInfo, vnet);

    for(Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort +1) begin
      //$display($time, "Value of saReqBits for output port %d is : %b", outPort , (saReqBits[outPort]));
      let partialRes <- outPortArbiter[outPort].getArbit(saReqBits[outPort]);
      grantedInPorts = grantedInPorts | partialRes;
      //$display($time, "Winner for output port %d is : %b", outPort , grantedInPorts);
      grantedOutPorts[outPort] = (partialRes!=0)? 1:0;
    end

    grantedInPortsBuf.enq(grantedInPorts);
`ifdef SMART
    grantedOutPortsBuf.enq(grantedOutPorts);
`endif
  endrule

  /******************************* Interface ***********************************/

  method Bool isInited = inited;

  method Action reqSA(SAReq req, FreeVCInfo freeVCInfo,VNETreq vnet );

    $display($time, "    SAReq is : %b", (req));
    $display($time, "    FreeVCInfo is:  %b", (freeVCInfo));
    $display($time, "    VNETreq is : %b", (vnet));
    saReqBuf.enq(req);
    vNETreqBuf.enq(vnet);
    freeVCInfoBuf.enq(freeVCInfo);
  endmethod

  method ActionValue#(SARes) getGrantedInPorts;
    grantedInPortsBuf.deq;
      $display($time, "    Output port arbitration winners are is : %b", (grantedInPortsBuf.first));
      $display();
    return grantedInPortsBuf.first;
  endmethod
`ifdef SMART
  method ActionValue#(SARes) getGrantedOutPorts;
    grantedOutPortsBuf.deq;
    return grantedOutPortsBuf.first;
  endmethod
`endif
endmodule
