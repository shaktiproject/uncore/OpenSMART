/*
 Message Types

 Author: Hyoukjun Kwon(hyoukjun@gatech.edu)


*/


/********** Native Libraries ************/
import Vector::*;
/******** User-Defined Libraries *********/
import Types::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;

/************* Definitions **************/

//1. Sub-definitions for Flit class

  //Message class and Flit types
  //typedef enum {DataResp=0, Control0=1, Control1=2}              MsgType  deriving(Bits, Eq, FShow);
  typedef Bit#(TLog#(NumVNETs))  MsgType;
	typedef Bit#(TLog#(NumVNETs)) MsgTypeBit;
  typedef enum {Head, Body, Tail, HeadTail} FlitType deriving(Bits, Eq, FShow);

  //Statistic information for tests
  typedef struct {
//    Data      flitId;
    Data      hopCount;
    MeshWIdx  srcX;
    MeshHIdx  srcY;
    MeshWIdx  dstX;
    MeshHIdx  dstY;
    Data      injectedCycle;
    Data      inflightCycle;
  } FlitStatistics deriving(Bits, Eq, FShow);

`ifdef SOURCE_ROUTING
    typedef SourceRoutingInfo RouteInfo;
`else
    typedef LookAheadRouteInfo RouteInfo;
`endif

  typedef Vector#(NumPorts, Maybe#(RouteInfo)) RouteInfoBundle;

  typedef Data FlitData;

  typedef struct {
    VCIdx vc;
    RouteInfo routeInfo;
    MsgType msgType;
  } Header deriving (Bits, Eq, FShow);

//2. Main definition
  // Flit Type
  typedef struct {
    MsgType   msgType;
    VCIdx     vc;
    FlitType  flitType;  //Head, Body, Tail, HeadTail
    RouteInfo routeInfo;
    FlitData  flitData;
// This 'stat' should be discarded in the generated verilog unless needed for Performance Counter.
 `ifdef DETAILED_STATISTICS
    FlitStatistics stat;
 `endif
  } Flit deriving (Bits, Eq, FShow);

/* Bundles */
typedef Vector#(NumPorts, Maybe#(Header))   HeaderBundle;
typedef Vector#(NumPorts, Maybe#(FlitType)) FlitTypeBundle;
typedef Vector#(NumPorts, Maybe#(Flit))     FlitBundle;
typedef Bit#(NumVNETs)     HasVNET;

/// This function returns the VNETid for a message type.
// TODO: Parameterize it!
/*
function Bit#(2) getVnetID(MsgType msgType);
  if(msgType==DataResp)
    return 0;
  else if (msgType==Control0)
    return 1;
  else
    return 2;
endfunction
*/
function Bool isHead(Flit flit);
  return (flit.flitType == Head || flit.flitType == HeadTail);
endfunction

function Bool isTail(Flit flit);
  return (flit.flitType == Tail || flit.flitType == HeadTail);
endfunction

function Bool isValidFlitBundle (FlitBundle fb, DirIdx idx);
  return isValid(fb[idx]);
endfunction

function Flit fb_getFlit(FlitBundle fb, Integer idx);
  return validValue(fb[idx]);
endfunction

function Direction getFlitDirection(Flit flit);
`ifdef SOURCE_ROUTING
  return idx2Dir(flit.routeInfo[0]);
`else
  return flit.routeInfo.nextDir;
`endif
endfunction
