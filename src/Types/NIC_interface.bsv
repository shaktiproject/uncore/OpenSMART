package NIC_interface;

import Types ::*;
import MessageTypes ::*;
import CreditTypes ::*;

interface NetworkOuterInterface;
  method Action                     putFlit(Flit flit);
  method ActionValue#(Flit)         getFlit;
  method Action                     putCredit(CreditSignal crd);
  method ActionValue#(CreditSignal) getCredit;
endinterface

endpackage
