import Vector::*;

import Types::*;
import MessageTypes::*;
import SmartTypes::*;
import CreditTypes::*;
import RoutingTypes::*;
import VirtualChannelTypes::*;
import NIC_interface:: *;

`ifdef SMART
import SmartRouterTypes::*;
import SmartRouter::*;
import SSR_Manager::*;
//import SSR_Router::*;
//import SSR_Network::*;
`else
import BaselineRouter::*;
`endif


import Connectable::*;

interface Network;
  method Bool isInited;
  interface Vector#(MeshHeight, Vector#(MeshWidth, NetworkOuterInterface)) ntkPorts;
  `ifdef  monitor_link_utilisation
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumPorts,Bool))) monitor_link_utilisation;
  `endif
  // `ifdef monitor_hop_counts // encode hop count as destination id of outgoing flit. id = node_id implies no flit.
  //   (* always_ready *)
  //   method Vector#(MeshHeight,Vector#(MeshWidth,Bit#(TLog#(Mul#(MeshHeight,MeshWidth))))) monitor_traffic_latency;
  // `endif
  `ifdef monitor_flit_ingress
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_ingress;
  `endif
  `ifdef monitor_flit_egress
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_egress;
  `endif
endinterface

(* synthesize *)
module mkNetwork(Network);

  Reg#(Bool) inited <- mkReg(False);

`ifdef SMART
  //SSR managers
  Vector#(MeshHeight, Vector#(MeshWidth, SSR_Manager)) horizontal_SSR_Managers <- replicateM(replicateM(mkSSR_Manager));
  Vector#(MeshHeight, Vector#(MeshWidth, SSR_Manager)) vertical_SSR_Managers <- replicateM(replicateM(mkSSR_Manager));

//Tiles
  Vector#(MeshHeight, Vector#(MeshWidth, Router)) routers <- replicateM(replicateM(mkSmartRouter));
`else
  Vector#(MeshHeight, Vector#(MeshWidth, Router)) routers =?;
  Integer k=0;
  for(Integer i=0; i < valueOf(MeshHeight) ; i = i+1)begin
    for(Integer j=0; j < valueOf(MeshWidth) ; j = j+1)begin
      routers[i][j] <- mkBaselineRouter(fromInteger(k));
      k=k+1;
    end
  end
`endif

// performance monitoring signals - not exposing these will optimise these signalls out
  Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Wire#(Bool)))) vwr_monitor_node_ingress <- replicateM(replicateM(replicateM(mkDWire(False))));
  Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Wire#(Bool)))) vwr_monitor_node_egress  <- replicateM(replicateM(replicateM(mkDWire(False))));
  // Vector#(MeshHeight,Vector#(MeshWidth,Wire#(Bit#(TLog#(Mul#(MeshHeight,MeshWidth)))))) v_wr_monitor_traffic_latency;
  // for(Integer i=0; i < valueOf(MeshHeight) ; i = i+1)begin
  //   for(Integer j=0; j < valueOf(MeshWidth) ; j = j+1)begin
  //     v_wr_monitor_traffic_latency[i][j] <- mkDWire(truncate((i * valueOf(MeshWidth))+j));
  //   end
  // end

  rule doInitialize(!inited);
    Bit#(1) isFullyInited = 1;

    for(Integer i=0; i< valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j< valueOf(MeshWidth); j=j+1) begin
        isFullyInited = (isFullyInited ==1 && routers[i][j].isInited())? 1: 0;
      end
    end

    inited <= (isFullyInited == 1)? True:False;
  endrule

/* Interconnection among mesh nodes */
// The connection between node and NI is via external interface
//Horizontal connections; W->E and E->W
  for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin

    // W->E
    for(Integer j=0; j<valueOf(MeshWidth)-1; j=j+1) begin
      //Flit link
      //mkConnection(routers[i][j].dataLinks[dIdxEast].getFlit,
      //               routers[i][j+1].dataLinks[dIdxWest].putFlit);

      //Credit link
      //mkConnection(routers[i][j].controlLinks[dIdxEast].getCredit,
      //               routers[i][j+1].controlLinks[dIdxWest].putCredit);
      rule rl_data_cnn;
        $display($time, "\tI am DATA connected from west to east!!");
        let lv_data <- routers[i][j].dataLinks[dIdxEast].getFlit;
        routers[i][j+1].dataLinks[dIdxWest].putFlit(lv_data);
      endrule


      rule rl_credit_cnn;
        $display($time, "\tI am getting connected from west to east!!");
        let lv_credit <- routers[i][j].controlLinks[dIdxEast].getCredit;
        routers[i][j+1].controlLinks[dIdxWest].putCredit(lv_credit);
      endrule
    end

    // E->W
    for(Integer j=1; j<valueOf(MeshWidth); j=j+1) begin
      //Flit link
      mkConnection(routers[i][j].dataLinks[dIdxWest].getFlit,
                     routers[i][j-1].dataLinks[dIdxEast].putFlit);

      //Credit link
      mkConnection(routers[i][j].controlLinks[dIdxWest].getCredit,
                   routers[i][j-1].controlLinks[dIdxEast].putCredit);
    end
  end

  //Vertical connections; N->S and S->N
  for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
    // N->S
    for(Integer i=0; i<valueOf(MeshHeight)-1; i=i+1) begin
      //Flit link
      mkConnection(routers[i][j].dataLinks[dIdxSouth].getFlit,
                     routers[i+1][j].dataLinks[dIdxNorth].putFlit);

      //Credit link
      mkConnection(routers[i][j].controlLinks[dIdxSouth].getCredit,
                     routers[i+1][j].controlLinks[dIdxNorth].putCredit);
    end

    // S->N
    for(Integer i=1; i<valueOf(MeshHeight); i=i+1)
    begin
      //Flit link
      mkConnection(routers[i][j].dataLinks[dIdxNorth].getFlit,
                     routers[i-1][j].dataLinks[dIdxSouth].putFlit);

      //Credit link
      mkConnection(routers[i][j].controlLinks[dIdxNorth].getCredit,
                     routers[i-1][j].controlLinks[dIdxSouth].putCredit);
    end
  end

`ifdef SMART
  /* SSR Links */
  //Between SSR manager and Router
  for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
    for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin

    // W->E
    mkConnection(routers[i][j].controlLinks[dIdxEast].getSSR,
                   horizontal_SSR_Managers[i][j].routerChannel[0].putSSR);

    mkConnection(routers[i][j].controlLinks[dIdxEast].putSSRs,
                   horizontal_SSR_Managers[i][j].routerChannel[0].getSSRs);

    // E->W
    mkConnection(routers[i][j].controlLinks[dIdxWest].getSSR,
                   horizontal_SSR_Managers[i][j].routerChannel[1].putSSR);

    mkConnection(routers[i][j].controlLinks[dIdxWest].putSSRs,
                   horizontal_SSR_Managers[i][j].routerChannel[1].getSSRs);

    // N->S
    mkConnection(routers[i][j].controlLinks[dIdxSouth].getSSR,
                   vertical_SSR_Managers[i][j].routerChannel[0].putSSR);

    mkConnection(routers[i][j].controlLinks[dIdxSouth].putSSRs,
                   vertical_SSR_Managers[i][j].routerChannel[0].getSSRs);

    // S->N
    mkConnection(routers[i][j].controlLinks[dIdxNorth].getSSR,
                   vertical_SSR_Managers[i][j].routerChannel[1].putSSR);

    mkConnection(routers[i][j].controlLinks[dIdxNorth].putSSRs,
                   vertical_SSR_Managers[i][j].routerChannel[1].getSSRs);
    end
  end

  //Between neighboring managers
  for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
    // W->E
    for(Integer j=0; j<valueOf(MeshWidth)-1; j=j+1) begin
      mkConnection(horizontal_SSR_Managers[i][j].managerChannel[0].getSSRs,
                     horizontal_SSR_Managers[i][j+1].managerChannel[0].putSSRs);
    end

    // E->W
    for(Integer j=1; j<valueOf(MeshWidth); j=j+1) begin
        mkConnection(horizontal_SSR_Managers[i][j].managerChannel[1].getSSRs,
                       horizontal_SSR_Managers[i][j-1].managerChannel[1].putSSRs);
    end
  end

  //Vertical connections; N->S and S->N
  for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
    // N->S
    for(Integer i=0; i<valueOf(MeshHeight)-1; i=i+1) begin
      mkConnection(vertical_SSR_Managers[i][j].managerChannel[0].getSSRs,
                     vertical_SSR_Managers[i+1][j].managerChannel[0].putSSRs);
    end

    // S->N
    for(Integer i=1; i<valueOf(MeshHeight); i=i+1) begin
        mkConnection(vertical_SSR_Managers[i][j].managerChannel[1].getSSRs,
                       vertical_SSR_Managers[i-1][j].managerChannel[1].putSSRs);
    end
  end

  for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
    rule rl_removeEdgeSSRs_Left(inited);
      let sb <- horizontal_SSR_Managers[i][0].managerChannel[1].getSSRs;
    endrule

    rule rl_injectEdgeSSRs_Left(inited);
      SSRBundle dummy = replicate(0);
      horizontal_SSR_Managers[i][0].managerChannel[0].putSSRs(dummy);
    endrule


    rule rl_removeEdgeSSRs_Right(inited);
      let sb <- horizontal_SSR_Managers[i][fromInteger(valueOf(MeshWidth))-1].managerChannel[0].getSSRs;
    endrule

    rule rl_injectEdgeSSRs_Right(inited);
      SSRBundle dummy = replicate(0);
      horizontal_SSR_Managers[i][fromInteger(valueOf(MeshWidth))-1].managerChannel[1].putSSRs(dummy);
    endrule
  end

  for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
    rule rl_removeEdgeSSRs_Above(inited);
      let sb <- vertical_SSR_Managers[0][j].managerChannel[1].getSSRs;
    endrule

    rule rl_injectEdgeSSRs_Above(inited);
      SSRBundle dummy = replicate(0);
      vertical_SSR_Managers[0][j].managerChannel[0].putSSRs(dummy);
    endrule

    rule rl_removeEdgeSSRs_Below(inited);
      let sb <- vertical_SSR_Managers[fromInteger(valueOf(MeshHeight))-1][j].managerChannel[0].getSSRs;
    endrule

    rule rl_injectEdgeSSRs_Below(inited);
      SSRBundle dummy = replicate(0);
      vertical_SSR_Managers[fromInteger(valueOf(MeshHeight))-1][j].managerChannel[1].putSSRs(dummy);
    endrule
  end
`endif

/* External Interfaces */
// Used to establish connection between NI and local input and output ports
  Vector#(MeshHeight, Vector#(MeshWidth, NetworkOuterInterface)) extInfc = newVector;
  for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
    for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
      extInfc[i][j] =
        interface NetworkOuterInterface
          method Action putFlit(Flit flit) if(inited);
            routers[i][j].dataLinks[dIdxLocal].putFlit(flit);
            let lv_vcid = flit.vc;
            vwr_monitor_node_ingress[i][j][lv_vcid] <= True;
            //v_wr_monitor_traffic_latency[i][j] <= flit.
          endmethod

          method ActionValue#(Flit) getFlit if(inited);
            let flit <- routers[i][j].dataLinks[dIdxLocal].getFlit;
            let lv_vcid = flit.vc;
            vwr_monitor_node_egress[i][j][lv_vcid] <= True;
            return flit;
          endmethod

          method Action putCredit(CreditSignal crd);
            routers[i][j].controlLinks[dIdxLocal].putCredit(crd);
          endmethod

          method ActionValue#(CreditSignal) getCredit;
            let credit <- routers[i][j].controlLinks[dIdxLocal].getCredit;
            return credit;
          endmethod
        endinterface;
    end
  end
  interface ntkPorts = extInfc;

  method Bool isInited;
    return inited;
  endmethod

// performance monitoring interfaces
`ifdef  monitor_link_utilisation
  method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumPorts,Bool))) monitor_link_utilisation;
    Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumPorts,Bool))) vlv_aggregate_link_utilisation = replicate(replicate(replicate(False)));
    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        vlv_aggregate_link_utilisation[i][j] = routers[i][j].monitor_link_utilisation;
      end
    end
    return vlv_aggregate_link_utilisation;
  endmethod
`endif
//`ifdef monitor_hop_counts // encode hop count as destination id of outgoing flit. id = node_id implies no flit.
//  (* always_ready *)
//  method Vector#(MeshHeight,Vector#(MeshWidth,Bit#(TLog#(Mul#(MeshHeight,MeshWidth))))) monitor_traffic_latency;
//`endif
`ifdef monitor_flit_ingress
  method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_ingress;
    Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) vlv_monitor_node_ingress = replicate(replicate(replicate(False)));
    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        for(Integer k=0; k<valueOf(NumVCs); k=k+1) begin
            vlv_monitor_node_ingress[i][j][k] = vwr_monitor_node_ingress[i][j][k] ;
        end
      end
    end
    return vlv_monitor_node_ingress;
  endmethod
`endif
`ifdef monitor_flit_egress
  method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_egress;
    Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) vlv_monitor_node_egress = replicate(replicate(replicate(False)));
    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        for(Integer k=0; k<valueOf(NumVCs); k=k+1) begin
          vlv_monitor_node_egress[i][j][k] = vwr_monitor_node_egress[i][j][k] ;
        end
      end
    end
    return vlv_monitor_node_egress;
  endmethod
`endif
endmodule


