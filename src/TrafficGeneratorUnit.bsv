import Vector::*;
import Fifo::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;
import CreditTypes::*;

`ifdef LFSR
import TrafficGeneratorLFSR :: * ;
`else
import TrafficGenerator::*;
`endif

import SmartVCAllocUnit::*;

interface TrafficGeneratorUnit;
  `ifdef LFSR
  method Action initialize(MeshHIdx yID, MeshWIdx xID, Bit#(32) seed);
  `else
  method Action initialize(MeshHIdx yID, MeshWIdx xID);
  `endif
  method Action genFlit(Data clkCount);
  method ActionValue#(Flit) getFlit;
  method Action putVC(CreditSignal sig);
endinterface

(* synthesize *)
module mkTrafficGeneratorUnit(TrafficGeneratorUnit);
  SmartVCAllocUnit vcAllocUnit <- mkSmartVCAllocUnit;
  TrafficGenerator trafficGenerator <- mkUniformRandom;
//  TrafficGenerator trafficGenerator <- mkBitComplement;

  Fifo#(NumTrafficGeneratorBufferSlots, Flit) tempFifo <- mkBypassFifo;
  Fifo#(1, Flit) outFifo <- mkBypassFifo;

/*
  rule rl_getFlit;
    let flit <- trafficGenerator.getFlit;
    tempFifo.enq(flit);
  endrule
*/

  rule rl_getVC;
    let flit = tempFifo.first;
    tempFifo.deq;
    // flit.msgType = Data;
    let vc <- vcAllocUnit.getNextVC(flit.msgType);
    flit.vc = vc;
    outFifo.enq(flit);
    // To check the message type and other fields of the generated flit.
    //$display("Default value of message type in TrafficGeneratorUnit is : ",fshow(flit) );
  endrule

  method Action genFlit(Data clkCount);
    let flit <- trafficGenerator.getFlit;
    `ifdef DETAILED_STATISTICS
    flit.stat.injectedCycle = clkCount;
    `endif
    tempFifo.enq(flit);
  endmethod

`ifdef LFSR
  method Action initialize(MeshHIdx yID, MeshWIdx xID, Bit#(32) seed);
    trafficGenerator.initialize(yID, xID, seed);
  endmethod
`else
  method Action initialize(MeshHIdx yID, MeshWIdx xID);
    trafficGenerator.initialize(yID, xID);
  endmethod
`endif

  method ActionValue#(Flit) getFlit;
    outFifo.deq;
    return outFifo.first;
  endmethod

  method Action putVC(CreditSignal sig);
    if(isValid(sig)) begin
      vcAllocUnit.putFreeVC(validValue(sig).msgType,validValue(sig).vc);
    end
  endmethod

endmodule
