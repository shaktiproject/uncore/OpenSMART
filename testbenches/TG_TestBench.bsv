package TG_TestBench;
import Vector::*;
import Fifo::*;
import Connectable::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;
import CreditTypes::*;
import NIC_interface::*;


import Network::*;
import TrafficGeneratorUnit::*;
import TrafficGeneratorBuffer::*;
import CreditUnit::*;
import StatLogger::*;

interface Ifc_TG_TestBench;
  `ifdef  monitor_link_utilisation
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumPorts,Bool))) monitor_link_utilisation;
  `endif
  // `ifdef monitor_hop_counts // encode hop count as destination id of outgoing flit. id = node_id implies no flit.
  //   (* always_ready *)
  //   method Vector#(MeshHeight,Vector#(MeshWidth,Bit#(TLog#(Mul#(MeshHeight,MeshWidth))))) monitor_traffic_latency;
  // `endif
  `ifdef monitor_flit_ingress
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_ingress;
  `endif
  `ifdef monitor_flit_egress
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_egress;
  `endif
endinterface

//(* synthesize *)
module mkTG_TestBench(Ifc_TG_TestBench);

  /********************************* States *************************************/
  Reg#(Data) clkCount  <- mkReg(0);
  Reg#(Bool) inited    <- mkReg(False);
  Reg#(Data) initCount <- mkReg(0);


  Vector#(MeshHeight, Vector#(MeshWidth, TrafficGeneratorUnit))    trafficGeneratorUnits       <- replicateM(replicateM(mkTrafficGeneratorUnit));
  Vector#(MeshHeight, Vector#(MeshWidth, TrafficGeneratorBuffer))  trafficGeneratorBufferUnits <- replicateM(replicateM(mkTrafficGeneratorBuffer));

  Vector#(MeshHeight, Vector#(MeshWidth, ReverseCreditUnit))  creditUnits    <- replicateM(replicateM(mkReverseCreditUnit));
  Vector#(MeshHeight, Vector#(MeshWidth, StatLogger))         statLoggers    <- replicateM(replicateM(mkStatLogger));

  /******************************** Submodule ************************************/

  Network meshNtk <- mkNetwork;

  rule init(!inited);
    if(initCount == 0) begin
      clkCount <= 0;
      Integer k=1;
      for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
        for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
          k=k+1;
          // initializing the source node for traffic
         `ifdef LFSR
          trafficGeneratorUnits[i][j].initialize(fromInteger(i), fromInteger(j), fromInteger(k));
          `else
          trafficGeneratorUnits[i][j].initialize(fromInteger(i), fromInteger(j));
          `endif
        end
      end
    end

    initCount <= initCount + 1;
    if(meshNtk.isInited && initCount > fromInteger(valueOf(MeshHeight)) + fromInteger(valueOf(MeshWidth)))  begin
      inited <= True;
    end
  endrule

  rule doClkCount(inited && clkCount < fromInteger(valueOf(BenchmarkCycle)));
    if(clkCount % 10000 == 0) begin
      $display("Elapsed Simulation Cycles: %d",clkCount);
    end
    clkCount <= clkCount + 1;
  endrule

  rule finishBench(inited && clkCount == fromInteger(valueOf(BenchmarkCycle)));
      Data res = 0;
      Data send = 0;
      Data hop = 0;
      Data remainingFlits = 0;
      Data inflight = 0;
      Bit#(128) resLatency = 0;

			Vector#(NumVNETs, Data) sendCount = ?;
			Vector#(NumVNETs, Data) recvCount = ?;

      for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
        for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
          sendCount = statLoggers[i][j].getSendCount;
          recvCount = statLoggers[i][j].getRecvCount;
          Data latencyCount = statLoggers[i][j].getLatencyCount;
          Data remFlits = trafficGeneratorBufferUnits[i][j].getRemainingFlitsNumber;
//          Data extraLatency = trafficGeneratorBufferUnits[i][j].getExtraLatency(clkCount);
          Data hopCount = statLoggers[i][j].getHopCount;
          Data inflightCycle = statLoggers[i][j].getInflightLatencyCount;
					for(Integer k=0; k<valueOf(NumVNETs); k=k+1) begin
							//$display("send_count[%2d][%2d] VNET0 = %5d  VNET1 = %5d  VNET2 = %5d,   recv_count[%2d][%2d] VNET0 = %5d  VNET1 = %5d  VNET2 = %5d", 
					//i, j, sendCount[0], sendCount[1],sendCount[2], i, j, recvCount[0], recvCount[1], recvCount[2]);
						$display("Injected by ROUTER[%2d][%2d] in VNET[%2d]: %5d],   Ejected by ROUTER[%2d][%2d] from  VNET[%2d]: %5d", i, j, k, sendCount[k], i, j, k, recvCount[k]);
						send = send + sendCount[k];
						res = res + recvCount[k];
				  end
          resLatency = resLatency + zeroExtend(latencyCount);// + zeroExtend(extraLatency);
          remainingFlits = remainingFlits + remFlits;
          hop = hop + hopCount;
          inflight = inflight + inflightCycle;
	    end
      end

			//Vector#(NumVNETs, Data) sendCount = ?;
			//Vector#(NumVNETs, Data) recvCount = ?;
			//for(Integer k=0; k<valueOf(NumVNETs); k=k+1) begin
			//		sendCount[k] = 0;
			//		recvCount[k] = 0;
			//end

			//for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
			//		for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
			//				for(Integer k=0; j<valueOf(NumVNETs); k=k+1) begin
			//						sendCount[k] = sendCount[k] + statLoggers[i][j].getSendCount[k];
			//						recvCount[k] = recvCount[k] + statLoggers[i][j].getRecvCount[k];
			//				end
			//		end
			//end
		  
			//for(Integer k=0; k<valueOf(NumVNETs); k=k+1) begin
			//		$display("Total injected packets for VNET %d : %d", k, sendCount[k]);
			//		$display("Total received packets for VNET %d : %d", k, recvCount[k]);
			//end

      $display("Elapsed clock cycles: %d", clkCount);
      $display("Total injected packet: %d",send);
      $display("Total received packet: %d",res);
      $display("Total latency: %d", resLatency);
      $display("Total hopCount: %d", hop);
      $display("Total inflight latency: %d", inflight);
      $display("Number of remaiing Flits in traffic generator side: %d", remainingFlits);
      $finish;
  endrule


  //Credit Links
  for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
    for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin

      mkConnection(creditUnits[i][j].getCredit,
                     meshNtk.ntkPorts[i][j].putCredit);

      mkConnection(meshNtk.ntkPorts[i][j].getCredit,
                     trafficGeneratorUnits[i][j].putVC);
    end
  end


  //Data Links
  for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
    for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin

      rule genFlits(inited);
        trafficGeneratorUnits[i][j].genFlit(clkCount);
      endrule

      rule prepareFlits(inited);
        let flit <- trafficGeneratorUnits[i][j].getFlit;
        trafficGeneratorBufferUnits[i][j].putFlit(flit);//, clkCount);
      endrule

      rule putFlits(inited); // inject flit into the network
        let flit <- trafficGeneratorBufferUnits[i][j].getFlit;

        `ifdef DETAILED_STATISTICS
        flit.stat.inflightCycle = clkCount;
        `endif

        meshNtk.ntkPorts[i][j].putFlit(flit);
        //$display("%10d",$time, "\tInjecting flit from [%2d,%2d] ", j,i,fshow(flit.stat));
        statLoggers[i][j].incSendCount(flit.msgType);
      endrule

      rule getFlits(inited);// eject flit from the network (SINK)
        let flit <- meshNtk.ntkPorts[i][j].getFlit;

        `ifdef DETAILED_STATISTICS
        Data hopCount = flit.stat.hopCount;
        `endif

//        $display($time,"\tReceiving flit from [%d,%d] ", j,i,fshow(flit.stat));
/*
        if((flit.stat.dstX != fromInteger(j)) || (flit.stat.dstY != fromInteger(i))) begin
            $display("Warning: Missrouted.\n Received from(%d, %d) but the destination is (%d, %d) source: (%d, %d)", fromInteger(i), fromInteger(j), flit.stat.dstY, flit.stat.dstX, flit.stat.srcY, flit.stat.srcX);
        end

        else begin
            $display("Correct\n Received from(%d, %d). The destination is (%d, %d)  source: (%d, %d)", fromInteger(i), fromInteger(j), flit.stat.dstY, flit.stat.dstX, flit.stat.srcY, flit.stat.srcX);
        end
*/
        `ifdef DETAILED_STATISTICS
        statLoggers[i][j].incLatencyCount(clkCount - flit.stat.injectedCycle);
        statLoggers[i][j].incInflightLatencyCount(clkCount - flit.stat.inflightCycle);
        statLoggers[i][j].incHopCount(hopCount);
       `endif
        statLoggers[i][j].incRecvCount(flit.msgType);
        $display($time, "\ttestbench calling putCredit");
        creditUnits[i][j].putCredit(Valid(CreditSignal_{vc: flit.vc, isTailFlit: True, msgType: flit.msgType}));
      endrule

    end
  end
  `ifdef  monitor_link_utilisation
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumPorts,Bool))) monitor_link_utilisation = meshNtk.monitor_link_utilisation;
  `endif
  // `ifdef monitor_hop_counts // encode hop count as destination id of outgoing flit. id = node_id implies no flit.
  //   (* always_ready *)
  //   method Vector#(MeshHeight,Vector#(MeshWidth,Bit#(TLog#(Mul#(MeshHeight,MeshWidth))))) monitor_traffic_latency;
  // `endif
  `ifdef monitor_flit_ingress
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_ingress = meshNtk.monitor_node_ingress;
  `endif
  `ifdef monitor_flit_egress
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_egress = meshNtk.monitor_node_egress;
  `endif

endmodule
endpackage : TG_TestBench