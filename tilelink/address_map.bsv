package address_map;

import RoutingTypes::*;
import Types::*;

`include "system.defines"

typedef enum { A, B, C, D, E} Channel deriving(Bits, Eq);

function LookAheadRouteInfo address_map(Bit#(a) address, Channel ch, 
																				MeshWIdx x_co, MeshHIdx y_co, 
																				Bit#(2) source, Bit#(5) sink); 
	MeshWIdx xdest=0;
	MeshHIdx ydest=0;
	LookAheadRouteInfo routeInfo;
	if(ch==A || ch==C) begin
		if(address >= 'h80000000	 && address < 'h81000000) begin
			xdest = 1;	
			ydest = 0;	
		end
		if(address >= 'h81000000	 && address < 'h82000000) begin
			xdest = 1;	
			ydest = 1;	
		end
		if(address >= 'h82000000	 && address < 'h83000000) begin
			xdest = 1;	
			ydest = 2;	
		end
		if(address >= 'h83000000	 && address < 'h84000000) begin
			xdest = 1;	
			ydest = 3;	
		end
	end
	if(ch==B || ch==D) begin
		if(source==0) begin
			xdest = 0;	
			ydest = 0;	
		end
		if(source==1) begin
			xdest = 0;	
			ydest = 1;	
		end
		if(source==1) begin
			xdest = 0;	
			ydest = 2;	
		end
		if(source==1) begin
			xdest = 0;	
			ydest = 3;	
		end
	end

	routeInfo.dirX = (xdest>x_co)? WE_:EW_; 
  routeInfo.numXhops = (xdest > x_co)? (xdest-x_co) : (x_co-xdest);	
	routeInfo.dirY = (ydest>y_co)? NS_:SN_; 
	routeInfo.numYhops = (ydest > y_co)? (xdest-x_co) : (x_co-xdest);
  if(xdest != x_co) begin// Initial direction: X                                                 
    routeInfo.nextDir = (xdest > x_co)? east_ : west_;                                      
  end                                                                                           
  else if(ydest !=y_co) begin //Initial direction: Y                                             
    routeInfo.nextDir = (xdest > x_co)? south_:north_;                                      
  end                                                                                           
  else begin                                                                                    
    routeInfo.nextDir = local_;   
	end

	return routeInfo;
endfunction

endpackage
