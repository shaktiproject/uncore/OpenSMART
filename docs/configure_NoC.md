# Configuring the NoC

The OpenSMART is a highly-configurable NoC. 

## Configuring the Network : In src/Types/Types.bsv


* __DataSz__ : This variable defines the size of Data which can be carried by a flit.
    * Note : size of a flit is determined by adding all fields in struct of Field (file : src/Types/MessageTypes.bsv)

* __BenchmarkCycle__ : This variable defines the simulation time.

* __NumVNETs__ : This variable defines the total number of VNETs or message classes.

* __VCperVNET__ : This variable defines the total number of VCs per VNET.

* __MeshWidth__ : This variable defines the width of mesh topology. 

* __MeshHeight__ : This variable defines the height of mesh topology. 

* __NumUserVCs__ : This variable defines the total number of VCs per input port. It is NumVNETs * VCperVNET

* __InjectionRate__ : This variable defines the number of packets injected per node per cycle.

## Running the network: 

* With traffic generator testbench : 
```bash
make generate_verilog link_verilator simulate TOP_DIR=testbenches TOP_FILE=TG_TestBench.bsv TOP_MODULE=mkTG_TestBench
```

